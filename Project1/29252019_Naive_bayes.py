import string
exist = 0

positive_dict = {}

negative_dict = {}

positive_bag = []

negative_bag = []

bag_of_words = []

positive_sentences = []

negative_sentences = []

total_sentences = []

neg_probalities = []

pos_probalities = []

vocab = {}

def sentences(file):
    with open(file, 'r') as f:
        for line in f:
            line = line.strip("\n\t")
            if line[-1] == "1":
                positive_sentences.append(line)
            elif line[-1] == "0":
                negative_sentences.append(line)

    total_sentences.extend(positive_sentences)
    negative_sentences.extend(negative_sentences)

    f.close()

def bagOfWords(file):
    with open(file, 'r') as f:
        for line in f:
            line = line.strip("\n\t")
            if line[-1] == '1':
                for word in line.split():
                    positive_bag.append(word)
                    if word in positive_dict:
                        positive_dict[word.lower()] += 1
                    else:
                        positive_dict[word.lower()] = 1
            elif line[-1] == '0':
                for word in line.split():
                    negative_bag.append(word)
                    if word in negative_dict:
                        negative_dict[word.lower()] += 1
                    else:
                        negative_dict[word.lower()] = 1

        # positive_dict.pop("1")
        # negative_dict.pop("0")
        bag_of_words.extend(positive_bag)
        bag_of_words.extend(negative_bag)

    f.close()

    return bag_of_words

def vocabulary(file):
    with open(file, 'r') as f:
        for line in f:
            line = line.strip("\n\t")
            for word in line.split():
                if word in vocab:
                    vocab[word.lower()] += 1
                else:
                    vocab[word.lower()] = 1
        vocab.pop("0")
        vocab.pop("1")
        f.close()

def prior_probabilities():
    pos_prior_prob = len(positive_sentences)/len(total_sentences)
    neg_prior_prob = len(negative_sentences)/len(total_sentences)

    return  pos_prior_prob, neg_prior_prob


def likelihood_denominator():
    positive_denominator = len(positive_bag) + len(bag_of_words)
    negative_denominator = len(negative_bag) + len(bag_of_words)

    return negative_denominator, positive_denominator


def positive_test(file):
    pos_prior = prior_probabilities()[0]
    pos_denominator = likelihood_denominator()[0]

    for line in file:
        cummulative = 1
        split = line.split()
        for words in split:
            if(words in vocab and words in positive_dict):
                freq = positive_dict[words] + 1
            elif(words in vocab):
                freq = vocab[words] = 1
            else:
                continue
            cummulative *= (freq/pos_denominator)
        cummulative *= pos_prior
        pos_probalities.append(cummulative)
        return pos_probalities

def negative_test(file):
    neg_prior = prior_probabilities()[1]
    neg_denominator = likelihood_denominator()[1]

    for line in file:
        cummulative = 1
        split = line.split()
        for words in split:
            if(words in vocab and words in negative_dict):
                freq = negative_dict[words] + 1
            elif(words in vocab):
                freq = vocab[words] = 1
            else:
                continue
            cummulative *= (freq/neg_denominator)
        cummulative *= neg_prior
        neg_probalities.append(cummulative)

        return neg_probalities

def test(testFile):
    positive = positive_test(testFile)
    negative = negative_test(testFile)
    results = open("results.txt", "w+")
    for i in range(len(positive)):
        if positive[i] > negative[i]:
            results.write('1' + '\n')
        elif negative[i] > positive[i]:
            results.write('0' + '\n')

    results.close()

    return
def main():
    file = "bigTrain.txt"
    sentences(file)
    bagOfWords(file)
    vocabulary(file)
    prior_probabilities()
    likelihood_denominator()
    test_file = input(str("Please input your test file:"))
    test(test_file)



main()