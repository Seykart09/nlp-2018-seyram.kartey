
# coding: utf-8

# In[4]:


import pandas as panda
import nltk
import sklearn
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
import sklearn.naive_bayes as naive_bayes
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
import sys


# In[5]:

#Setting TrainFile to bigTrain.txt which contains all textfiles
file = "bigTrain.txt"


# In[6]:


#Function to train a normalized naive_bayes data set with a training file parameter
def train_nbn():
    #Creating a document separated by sentences and reactions(0 and 1)
    document = panda.read_csv(file, sep = "\t", names = ['sentences', 'reaction'])
    
    #using stopwords to normalize the sentences
    setOfWords = set(stopwords.words("english"))
    normalizer = TfidfVectorizer(use_idf=True, lowercase=True, strip_accents="ascii", stop_words=setOfWords)

    categorizer = document.reaction

    token = normalizer.fit_transform(document.sentences)
    
    #training and testing the normalized tokens and categories
    train_token, test_token, train_categorizer, test_categorizer = train_test_split(token, categorizer, random_state = 40)

    #Using multinomial naivebayes to train the data
    trainData = naive_bayes.MultinomialNB()

    trainData.fit(train_token, train_categorizer)

    print("normalized Naive Baye's accuracy %2.2f" % accuracy_score(test_categorizer, trainData.predict(test_token)))

    print("normalized Naive Baye's accuracy %2.2f" % roc_auc_score(test_categorizer, trainData.predict(test_token)))

    #returning the normalized Data and the Trained Data
    return normalizer, trainData


# In[7]:


#Testing the naive baye's with a test file
def test_nbn(testFile):

    normalizer, trainData = train_nbn()
    
    #opening the testfile, normalizing and predicting the data
    file = open(testFile, "r")
    predict_array = []
    for line in file:
        review_array = panda.np.array([line])
        normal = normalizer.transform(review_array)
        indicate_cat = trainData.predict(normal)

        predict_array.append(indicate_cat)
    
    #Creating the Results file and writing to it
    resultsFile = open("results-nb-n.txt", "w")

    for i in predict_array:
        result = str(i)
        resultsFile.write(result.strip("[]") + "\n")

    resultsFile.close()


# In[8]:


#Function to train an unnormalized naive_bayes data set with a training file parameter
def train_nbu():
    #Creating a document separated by sentences and reactions(0 and 1)
    document = panda.read_csv(file, sep = "\t", names = ['sentences', 'reaction'])
    
    #normalizer just retains all capitals and accents
    normalizer = TfidfVectorizer(use_idf=False, lowercase=False, strip_accents=False)

    categorizer = document.reaction

    token = normalizer.fit_transform(document.sentences)
    
    #training and testing the tokens and categories
    train_token, test_token, train_categorizer, test_categorizer = train_test_split(token, categorizer, random_state = 40)
    
    #Using multinomial naivebayes to train the data
    trainData = naive_bayes.MultinomialNB()
    trainData.fit(train_token, train_categorizer)

    # print("Unnormalized Naive Baye's accuracy %2.2f" % accuracy_score(test_categorizer, trainData.predict(test_token)))

    print("Unnormalized Naive Baye's accuracy %2.2f" % roc_auc_score(test_categorizer, trainData.predict(test_token)))

    #returning the Data and the Trained Data
    return normalizer, trainData


# In[9]:


#Testing the naive baye's with a test file
def test_nbu(testFile):

    normalizer, train = train_nbu()
    
    #opening the testfile, normalizing and predicting the data
    file = open(testFile, "r")
    predict_array = []
    for line in file:
        review_array = panda.np.array([line])
        normal = normalizer.transform(review_array)
        indicate_cat = train.predict(normal)

        predict_array.append(indicate_cat)
    
    #Creating the Results file and writing to it
    resultsFile = open("results-nb-u.txt", "w")

    for i in predict_array:
        result = str(i)
        resultsFile.write(result.strip("[]") + "\n")

    resultsFile.close()


# In[11]:


#Function to train a normalized logistic regression data set
def train_lrn():
    #Creating a document separated by sentences and reactions(0 and 1)
    doc = panda.read_csv(file, sep = "\t", names = ['sentences', 'reaction'])

    #using stopwords to normalize the sentences
    setOfWords = set(stopwords.words("english"))
    normalizer = TfidfVectorizer(use_idf=True, lowercase=True, strip_accents="ascii", stop_words=setOfWords)

    categorizer = doc.reaction

    token = normalizer.fit_transform(doc.sentences)
    
    #training and testing the normalized tokens and categories
    train_token, test_token, train_categorizer, test_categorizer = train_test_split(token, categorizer, random_state = 40)
    
    #Using multinomial naivebayes to train the data
    trainData = LogisticRegression(penalty="l2", C = 1)

    trainData.fit(train_token, train_categorizer)

    print("normalized LogReg accuracy %2.2f" % roc_auc_score(test_categorizer, trainData.predict(test_token)))

    #returning the normalized Data and the Trained Data
    return normalizer, trainData


# In[12]:


#Testing the Logistic Regression with a test file
def test_lrn(testFile):
    normalizer, trainData = train_lrn()

    #opening the testfile, normalizing and predicting the data
    file = open(testFile, "r")
    predict_array = []
    for line in file:
        review_array = panda.np.array([line])
        normal = normalizer.transform(review_array)
        indicate_cat = trainData.predict(normal)

        predict_array.append(indicate_cat)

    #Creating the Results file and writing to it
    resultsFile = open("results-lr-n.txt", "w")

    for i in predict_array:
        result = str(i)
        resultsFile.write(result.strip("[]") + "\n")

    resultsFile.close()


# In[13]:


#Function to train an unnormalized logistic regression data set
def train_lru():
    #Creating a document separated by sentences and reactions(0 and 1)
    doc = panda.read_csv(file, sep = "\t", names = ['sentences', 'reaction'])

    #normalizer just retains all capitals and accents
    normalizer = TfidfVectorizer(use_idf=False, lowercase=False, strip_accents=False)

    categorizer = doc.reaction

    token = normalizer.fit_transform(doc.sentences)
    
    #training and testing the tokens and categories
    train_token, test_token, train_categorizer, test_categorizer = train_test_split(token, categorizer, random_state = 40)
    
    #Using multinomial naivebayes to train the data
    trainData = LogisticRegression(penalty="l2", C = 1)

    trainData.fit(train_token, train_categorizer)

    print("Unnormalized LogReg accuracy %2.2f" % roc_auc_score(test_categorizer, trainData.predict(test_token)))

    #returning the Data and the Trained Data
    return normalizer, trainData


# In[14]:


#Testing the Logistic Regression with a test file
def test_lru(testFile):
    normalizer, trainData = train_lru()

    #opening the testfile, normalizing and predicting the data
    file = open(testFile, "r")
    predict_array = []
    for line in file:
        review_array = panda.np.array([line])
        normal = normalizer.transform(review_array)
        indicate_cat = trainData.predict(normal)

        predict_array.append(indicate_cat)

    #Creating the Results file and writing to it
    resultsFile = open("results-lr-u.txt", "w")

    for i in predict_array:
        result = str(i)
        resultsFile.write(result.strip("[]") + "\n")

    resultsFile.close()


# In[ ]:


if sys.argv[1] == "nb" and sys.argv[2] == "n":
    test_file = sys.argv[3]
    print("\n")
    print("######## Normalized Naive Bayes Classifier########")
    test_nbn(test_file)

elif sys.argv[1] == "nb" and sys.argv[2] == "u":
    test_file = sys.argv[3]
    print("\n")
    print("######## Unnormalized Naive Bayes Classifier########")
    test_nbu(test_file)

elif sys.argv[1] == "lr" and sys.argv[2] == "n":
    test_file = sys.argv[3]
    print("\n")
    print("######## Normalized Logistic Regression Classifier ########")
    test_lrn(test_file)

elif sys.argv[1] == "lr" and sys.argv[2] == "u":
    test_file = sys.argv[3]
    print("\n")
    print("######## Unnormalized Logistic Regression Classifier ########")
    test_lru(test_file)

