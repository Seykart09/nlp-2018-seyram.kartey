import pandas as panda
import nltk
import sklearn
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
import sklearn.naive_bayes as naive_bayes
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score


def train_nbu(file):
    document = panda.read_csv(file, sep = "\t", names = ['sentences', 'reaction'])
    
    normalizer = TfidfVectorizer(use_idf=False, lowercase=False, strip_accents=False)

    categorizer = document.reaction

    token = normalizer.fit_transform(document.sentences)

    train_token, test_token, train_categorizer, test_categorizer = train_test_split(token, categorizer, random_state = 40)

    trainData = naive_bayes.MultinomialNB()
    trainData.fit(train_token, train_categorizer)

    # print("Unnormalized Naive Baye's accuracy %2.2f" % accuracy_score(test_categorizer, trainData.predict(test_token)))

    print("Unnormalized Naive Baye's accuracy %2.2f" % roc_auc_score(test_categorizer, trainData.predict(test_token)))

    return normalizer, trainData


def test_nbu(trainFile,testFile):

    normalizer, train = train_nbu(trainFile)

    file = open(testFile, "r")
    predict_array = []
    for line in file:
        review_array = panda.np.array([line])
        normal = normalizer.transform(review_array)
        indicate_cat = train.predict(normal)

        predict_array.append(indicate_cat)

    resultsFile = open("results2.txt", "w")

    for i in predict_array:
        result = str(i)
        resultsFile.write(result.strip("[]") + "\n")

    resultsFile.close()


def main():
    trainFile = "bigTrain.txt"
    testFile = "test_sentences.txt"
    test_nbu(trainFile, testFile)


main()