import pandas as panda
import nltk
import sklearn
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
import sklearn.naive_bayes as naive_bayes
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score

def train_nbn(file):
    document = panda.read_csv(file, sep = "\t", names = ['sentences', 'reaction'])

    setOfWords = set(stopwords.words("english"))
    normalizer = TfidfVectorizer(use_idf=True, lowercase=True, strip_accents="ascii", stop_words=setOfWords)

    categorizer = document.reaction

    token = normalizer.fit_transform(document.sentences)

    train_token, test_token, train_categorizer, test_categorizer = train_test_split(token, categorizer, random_state = 40)

    trainData = naive_bayes.MultinomialNB()

    trainData.fit(train_token, train_categorizer)

    print("normalized Naive Baye's accuracy %2.2f" % accuracy_score(test_categorizer, trainData.predict(test_token)))

    print("normalized Naive Baye's accuracy %2.2f" % roc_auc_score(test_categorizer, trainData.predict(test_token)))


    return normalizer, trainData


def test_nbn(trainFile,testFile):

    normalizer, trainData = train_nbn(trainFile)

    file = open(testFile, "r")
    predict_array = []
    for line in file:
        review_array = panda.np.array([line])
        normal = normalizer.transform(review_array)
        indicate_cat = trainData.predict(normal)

        predict_array.append(indicate_cat)

    resultsFile = open("results.txt", "w")

    for i in predict_array:
        result = str(i)
        resultsFile.write(result.strip("[]") + "\n")

    resultsFile.close()


def main():
    trainFile = "bigTrain.txt"
    testFile = "test_sentences.txt"
    test_nbn(trainFile, testFile)


main()

